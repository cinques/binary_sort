
var tree = {};
var count = 0;

$("#add").click(function () {
    var value = $("#value");

    insert_to_tree(tree, value.val());
    update(tree);

    value.select();
    return false;
});

$("#clear").click(function () {
    tree = {};
    count = 0;
    update(tree);
});

$("#walk").click(function () {
    traverse(tree, highlight());
});