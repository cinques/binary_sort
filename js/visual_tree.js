(function () {
    
    const DURATION = 500;
    const WIDTH = 500;
    const HEIGHT = 500;

    // создаем svg для отрисовки векторной графики
    var svg = d3.select("body").append("svg")
        .attr("width", WIDTH)
        .attr("height", HEIGHT)
        .append("g")
        .attr("transform", "translate(20, 20)");

    // создаем свою функцию трансляции данных в древовидный d3 layout
    var d3tree = d3.layout.tree()
        .size([WIDTH - 10, HEIGHT - 50])
        .children(d => {
            if (!d.left && !d.right) {
                return null;
            }

            return [
                typeof(d.left) != 'undefined' ? d.left : d.left = {},
                typeof(d.right) != 'undefined' ? d.right : d.right = {}
            ];
        });

    // внешний вид связей между узлами дерева (стандартный от d3)
    var d3diag = d3.svg.diagonal();

    // обновление графика
    function update(data) {
        var t_nodes = d3tree(data);
        
        refresh_nodes(t_nodes);
        refresh_texts();
        refresh_circles();
        refresh_links(t_nodes);
    }

    function refresh_nodes(t_nodes) {
        // обновляем визуальные данные узлов в соответсвии с бизнес-логикой
        var nodes = svg.selectAll("g.node")
            .data(t_nodes, d => d.data.id || (d.data.id = count++));

        // nodes.enter() - добавленные узлы
        create_new_nodes(nodes.enter());

        // анимация добавления
        nodes.transition().duration(DURATION)
            .attr("transform", d => "translate(" + d.x + ", " + d.y + ")");

        remove_deleted_nodes(nodes.exit());
    }

    function create_new_nodes(new_nodes) {
        var g = new_nodes
            .append("svg:g")
            .attr("class", d => "node nd_" + d.data.id)
            .attr("transform", d => {
                var x = d.x;
                var y = d.y;
                if (d.parent) {
                    x = d.parent.x;
                    y = d.parent.y;
                }
                return "translate(" + x + ", " + y + ")";
            });

        // добавяем кружок вокруг текста
        g.append("svg:circle")
            .attr("r", d => Math.max(d.data.data ? d.data.data.toString().length * 5 : 15, 15))
            .style("stroke", d => d.data.data ? "#000" : "white");

        // добавляем текст
        g.append("svg:text")
            .text(d => d.data.data)
            .attr("dx", d => d.data.data ? -(d.data.data.toString().length * 5) : 0)
            .attr("dy", 5);
    }

    function remove_deleted_nodes(deleted_nodes) {
        deleted_nodes.transition()
            .style("fill", "white")
            .remove();
    }

    function refresh_texts() {
        svg.selectAll("g.node text")
            .text(d => d.data.data)
            .attr("dx", d => d.data.data ? -(d.data.data.toString().length * 5) : 0)
            .attr("dy", 5)
            .style("fill", "black");
    }

    function refresh_circles() {
         svg.selectAll("g.node circle")
            .attr("r", d => Math.max(d.data.data ? d.data.data.toString().length * 6 : 15, 15))
            .style("stroke", d => d.data.data ? "#000" : "white")
            .style("stroke-width", 1);
    }

    function refresh_links(t_nodes) {
        var links = svg.selectAll("path.link")
            .data(d3tree.links(t_nodes), d => d.source.data.id + "-" + d.target.data.id);

        links.style("stroke", d => d.target.data.data ? "black" : "white")
            .style("stroke-width", 1);

        create_new_links(links.enter());

        links.transition()
            .duration(DURATION)
            .attr("d", d3diag);

        remove_deleted_links(links.exit());
    }

    function create_new_links(new_links) {
        new_links
            .insert("svg:path", "g")
            .attr("class", d => "link ld_" + (d.source.data.id + "-" + d.target.data.id))
            .style("stroke", d => d.target.data.data ? "black" : "white")
            .attr("d", d => {
                var o = {x: d.source.x, y: d.source.y};
                return d3diag({source:o, target:o});
            })
            .transition()
            .duration(DURATION)
            .attr("d", d3diag);
    }

    function remove_deleted_links(deleted_links) {
        deleted_links.transition()
            .style("stroke", "white")
            .remove();
    }

    function highlight() {
        var delay = 0;
        return function (id) {
            svg.selectAll("g.nd_" + id + " circle")
            .transition()
            .delay(delay += 1000)
            .style("stroke", "red")
            .style("stroke-width", 3);
        };
    }

    window.update = update;
    window.highlight = highlight;
}());