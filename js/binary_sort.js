(function () {
    function insert_to_tree(root, value) {
        if (!root.data) {
            root.data = parseInt(value);
            if (root.id === undefined)
                root.id = count++;
        } else if (value > root.data) {
            insert_to_tree(root.right || (root.right = {}), value)
        } else {
            insert_to_tree(root.left || (root.left = {}), value)
        }
    }

    function traverse(root, callback) {
        if (root.left && root.left.data)
            traverse(root.left, callback);

        callback(root.id);

        if (root.right && root.right.data)
            traverse(root.right, callback);
    }

    window.insert_to_tree = insert_to_tree;
    window.traverse = traverse;
}());